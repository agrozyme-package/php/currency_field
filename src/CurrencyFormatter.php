<?php
declare(strict_types=1);

namespace Drupal\currency_field;

use CommerceGuys\Intl\Currency\Currency;
use CommerceGuys\Intl\Currency\CurrencyRepositoryInterface;
use CommerceGuys\Intl\Exception\InvalidArgumentException;
use CommerceGuys\Intl\Exception\UnknownCurrencyException;
use Drupal\price\CurrentLocaleInterface;

/**
 * Class CurrencyFormatter
 */
class CurrencyFormatter implements CurrencyFormatterInterface {
  protected $currencyRepository;
  protected $currencies = [];
  protected $defaultOptions = [
    'locale' => 'en',
    'use_grouping' => true,
    'style' => 'standard',
    'currency_display' => 'symbol',
  ];

  /**
   * CurrencyFormatter constructor.
   *
   * @param CurrencyRepositoryInterface $currencyRepository
   * @param CurrentLocaleInterface      $currentLocale
   */
  function __construct(CurrencyRepositoryInterface $currencyRepository, CurrentLocaleInterface $currentLocale) {
    $defaultOptions = ['locale' => $currentLocale->getLocale()->getLocaleCode()];
    $this->validateOptions($defaultOptions);

    $this->currencyRepository = $currencyRepository;
    $this->defaultOptions = array_replace($this->defaultOptions, $defaultOptions);
  }

  /**
   * Validates the provided options.
   * Ensures the absence of unknown keys, correct data types and values.
   *
   * @param array $options The options.
   *
   * @throws InvalidArgumentException
   */
  protected function validateOptions(array $options) {
    foreach ($options as $option => $value) {
      if (false === array_key_exists($option, $this->defaultOptions)) {
        throw new InvalidArgumentException(sprintf('Unrecognized option "%s".', $option));
      }
    }
    if (isset($options['use_grouping']) && (false === is_bool($options['use_grouping']))) {
      throw new InvalidArgumentException('The option "use_grouping" must be a boolean.');
    }
    if ((false === empty($options['style'])) && (false === in_array($options['style'], ['standard', 'accounting']))) {
      throw new InvalidArgumentException(sprintf('Unrecognized style "%s".', $options['style']));
    }
    if ((false === empty($options['currency_display'])) && (false === in_array($options['currency_display'], [
          'name',
          'code',
          'symbol',
          'none'
        ]))) {
      throw new InvalidArgumentException(sprintf('Unrecognized currency display "%s".', $options['currency_display']));
    }
  }

  /**
   * @inheritdoc
   */
  function format(string $currencyCode, array $options = []): string {
    $this->validateOptions($options);
    $options = array_replace($this->defaultOptions, $options);
    $currency = $this->getCurrency($currencyCode, $options['locale']);
    $item = '';

    switch ($options['currency_display']) {
      case 'name':
        $item = $currency->getName();
        break;
      case 'symbol':
        $item = $currency->getSymbol();
        break;
      case 'code':
        $item = $currency->getCurrencyCode();
        break;
      default:
        $item = '';
        break;
    }

    return $item;
  }

  /**
   * @inheritdoc
   */
  //  function parse($number, $currencyCode, array $options = []) {
  //    $this->validateOptions($options);
  //    $options = array_replace($this->defaultOptions, $options);
  //    $currency = $this->getCurrency($currencyCode, $options['locale']);
  //
  //    return $currencyCode;
  //  }

  /**
   * Gets the currency for the provided currency code and locale.
   *
   * @param string $currencyCode The currency code.
   * @param string $locale       The locale.
   *
   * @return Currency
   */
  protected function getCurrency($currencyCode, $locale) {
    if (false === isset($this->currencies[$currencyCode][$locale])) {
      try {
        $currency = $this->currencyRepository->get($currencyCode, $locale);
      } catch (UnknownCurrencyException $e) {
        // The requested currency was not found. Fall back
        // to a dummy object to show just the currency code.
        $currency = new Currency([
          'currency_code' => $currencyCode,
          'name' => $currencyCode,
          'numeric_code' => '000',
          'locale' => $locale,
        ]);
      }
      $this->currencies[$currencyCode][$locale] = $currency;
    }

    return $this->currencies[$currencyCode][$locale];
  }

}
