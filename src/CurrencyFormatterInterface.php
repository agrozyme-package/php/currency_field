<?php
declare(strict_types=1);

namespace Drupal\currency_field;

/**
 * Interface CurrencyFormatterInterface
 */
interface CurrencyFormatterInterface {
  /**
   * Formats a currency.
   * Supported options:
   * - locale:                  The locale. Default: 'en'.
   * - use_grouping:            Whether to use grouping separators,
   *                            such as thousands separators.
   *                            Default: true.
   * - style:                   The style.
   *                            One of: 'standard', 'accounting'.
   *                            Default: 'standard'.
   * - currency_display:        How the currency should be displayed.
   *                            One of: 'code', 'symbol', 'none'.
   *                            Default: 'symbol'.
   *
   * @param string $currencyCode The currency code.
   * @param array  $options      The formatting options.
   *
   * @return string The formatted currency.
   */
  function format(string $currencyCode, array $options = []): string;

  /**
   * Parses a formatted currency amount.
   * Commonly used in input widgets where the end-user might input
   * a number using digits and symbols common to their locale.
   * Supported options:
   * - locale: The locale. Default: 'en'.
   *
   * @param string $currencyCode The currency code.
   * @param array  $options      The parsing options.
   *
   * @return string|false The parsed number or FALSE on error.
   */
  //  function parse($currencyCode, array $options = []);
}
