<?php
declare(strict_types=1);

namespace Drupal\currency_field;

use InvalidArgumentException;

/**
 * Class Currency
 */
class Currency {
  protected $currencyCode;

  /**
   * Currency constructor.
   *
   * @param string $currency_code
   */
  function __construct(string $currency_code) {
    $this->assertCurrencyCodeFormat($currency_code);
    $this->currencyCode = strtoupper($currency_code);
  }

  /**
   * @return string
   */
  function getCurrencyCode() {
    return $this->currencyCode;
  }

  /**
   * @return string
   */
  function __toString() {
    return $this->currencyCode;
  }

  /**
   * @return array
   */
  function toArray() {
    return ['currency_code' => $this->currencyCode];
  }

  /**
   * @param string $currency_code
   */
  protected function assertCurrencyCodeFormat(string $currency_code) {
    if (3 !== strlen($currency_code)) {
      throw new InvalidArgumentException();
    }
  }

}
