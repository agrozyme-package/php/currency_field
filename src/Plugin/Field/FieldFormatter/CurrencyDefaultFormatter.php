<?php
declare(strict_types=1);

namespace Drupal\currency_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\currency_field\CurrencyFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'price_price_default' formatter.
 * @FieldFormatter(
 *   id = "currency_field_currency_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "currency_field_currency"
 *   }
 * )
 */
class CurrencyDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  protected $currencyFormatter;

  /**
   * {@inheritdoc}
   */
  function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    CurrencyFormatterInterface $currency_formatter
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('currency_field.currency_formatter'));
  }

  /**
   * {@inheritdoc}
   */
  static function defaultSettings() {
    return ['currency_display' => 'name'] + parent::defaultSettings();
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $elements['currency_display'] = [
      '#type' => 'radios',
      '#title' => $this->t('Currency display'),
      '#options' => [
        'name' => $this->t('Name (e.g. "US Dollar")'),
        'symbol' => $this->t('Symbol (e.g. "$")'),
        'code' => $this->t('Currency code (e.g. "USD")'),
        'none' => $this->t('None'),
      ],
      '#default_value' => $this->getSetting('currency_display'),
    ];

    return $elements;
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  function settingsSummary() {
    $summary = [];
    $currency_display = $this->getSetting('currency_display');
    $currency_display_options = [
      'name' => $this->t('Name (e.g. "US Dollar")'),
      'symbol' => $this->t('Symbol (e.g. "$")'),
      'code' => $this->t('Currency code (e.g. "USD")'),
      'none' => $this->t('None'),
    ];
    $summary[] = $this->t('Currency display: @currency_display.', [
      '@currency_display' => $currency_display_options[$currency_display],
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  function viewElements(FieldItemListInterface $items, $langcode) {
    $options = $this->getFormattingOptions();
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $this->currencyFormatter->format($item->currency_code, $options),
        '#cache' => [
          'contexts' => [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
            'price_country',
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * @return array
   */
  protected function getFormattingOptions() {
    return ['currency_display' => $this->getSetting('currency_display')];
  }

}
