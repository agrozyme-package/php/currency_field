<?php
declare(strict_types=1);

namespace Drupal\currency_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\currency_field\Plugin\Field\FieldType\CurrencyItem;

/**
 * Plugin implementation of the 'currency_field_currency_default' widget.
 * @FieldWidget(
 *   id = "currency_field_currency_default",
 *   label = @Translation("Currency"),
 *   field_types = {
 *     "currency_field_currency"
 *   }
 * )
 */
class CurrencyDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    /** @var CurrencyItem $item */
    $item = $items[$delta];

    if (false === $item->isEmpty()) {
      $element['#default_value'] = $item->toCurrency()->toArray();
    }

    $element['#type'] = 'currency_field_currency';
    $element['#available_currencies'] = array_filter($this->getFieldSetting('available_currencies'));
    //    dpm($element['#available_currencies']);
    return $element;
  }
}
