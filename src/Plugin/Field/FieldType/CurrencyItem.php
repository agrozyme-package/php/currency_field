<?php
declare(strict_types=1);

namespace Drupal\currency_field\Plugin\Field\FieldType;

use Drupal;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\currency_field\Currency;
use Drupal\price\Entity\CurrencyInterface;

/**
 * Plugin implementation of the 'currency' field type.
 *
 * @property string currency_code
 * @FieldType(
 *   id = "currency_field_currency",
 *   label = @Translation("Currency"),
 *   description = @Translation("Stores a three letter currency code."),
 *   category = @Translation("Price"),
 *   default_widget = "currency_field_currency_default",
 *   default_formatter = "currency_field_currency_default",
 * )
 */
class CurrencyItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['currency_code'] = DataDefinition::create('string')->setLabel(t('Currency code'))->setRequired(false);
    return $properties;
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  static function mainPropertyName() {
    return null;
  }

  /**
   * {@inheritdoc}
   */
  static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'currency_code' => [
          'description' => 'The currency code.',
          'type' => 'varchar',
          'length' => 3,
        ],
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  static function defaultFieldSettings() {
    return [
        'available_currencies' => [],
      ] + parent::defaultFieldSettings();
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    /** @var CurrencyInterface[] $currencies */
    $currencies = Drupal::entityTypeManager()->getStorage('price_currency')->loadMultiple();
    $options = [];

    foreach ($currencies as $index => $item) {
      $options[$index] = $item->getName();
    }

    $element = [];
    $element['available_currencies'] = [
      '#type' => count($options) < 10 ? 'checkboxes' : 'select',
      '#title' => $this->t('Available currencies'),
      '#description' => $this->t('If no currencies are selected, all currencies will be available.'),
      '#options' => $options,
      '#default_value' => $this->getSetting('available_currencies'),
      '#multiple' => true,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  function getConstraints() {
    $manager = Drupal::typedDataManager()->getValidationConstraintManager();
    $available_currencies = $this->getSetting('available_currencies');
    $constraints = parent::getConstraints();
    $constraints[] = $manager->create('Currency', ['availableCurrencies' => $available_currencies]);
    return $constraints;
  }

  /**
   * @noinspection PhpMissingParentCallCommonInspection
   * {@inheritdoc}
   */
  function isEmpty() {
    return empty($this->currency_code);
  }

  /**
   * {@inheritdoc}
   */
  function setValue($values, $notify = true) {
    if ($values instanceof Currency) {
      $currency = $values;
      $values = ['currency_code' => $currency->getCurrencyCode()];
    }
    parent::setValue($values, $notify);
  }

  /**
   * @return Currency
   */
  function toCurrency(): Currency {
    return new Currency($this->currency_code);
  }

}
