<?php
declare(strict_types=1);

namespace Drupal\currency_field\Element;

use Drupal;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\price\Entity\CurrencyInterface;
use InvalidArgumentException;

/**
 * Provides a price form element.
 * Usage example:
 *
 * @code
 * $form['currency'] = [
 *   '#type' => 'currency_field_currency',
 *   '#title' => $this->t('Currency'),
 *   '#default_value' => ['currency_code' => 'USD'],
 *   '#required' => TRUE,
 *   '#available_currencies' => ['USD', 'EUR'],
 * ];
 * @endcode
 * @FormElement("currency_field_currency")
 */
class Currency extends FormElement {

  /**
   * {@inheritdoc}
   */
  function getInfo() {
    $class = get_class($this);
    return [
      // List of currencies codes. If empty, all currencies will be available.
      '#available_currencies' => [],
      // The check is performed here so that it is cached.
      '#price_inline_errors' => Drupal::moduleHandler()->moduleExists('inline_form_errors'),
      '#default_value' => null,
      //      '#attached' => [
      //        'library' => ['price/admin'],
      //      ],
      '#element_validate' => [
        [$class, 'moveInlineErrors'],
      ],
      '#process' => [
        [$class, 'processElement'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
      '#input' => true,
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Builds the price_price form element.
   *
   * @param array                                $element
   *   The initial price_price form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array                                $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The built price_price form element.
   * @throws InvalidArgumentException
   *   Thrown when #default_value is not an instance of
   *   \Drupal\price\Price.
   */
  static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    $default_value = $element['#default_value'];
    if (isset($default_value) && !self::validateDefaultValue($default_value)) {
      throw new InvalidArgumentException('The #default_value for a price_price element must be an array with "currency_code" keys.');
    }

    /** @var ConfigEntityStorageInterface $currency_storage */
    $currency_storage = Drupal::service('entity_type.manager')->getStorage('price_currency');
    /** @var CurrencyInterface[] $currencies */
    $currencies = $currency_storage->loadMultiple();
    $currency_codes = array_keys($currencies);
    // Keep only available currencies.
    $available_currencies = $element['#available_currencies'];
    if (isset($available_currencies) && !empty($available_currencies)) {
      $currency_codes = array_intersect($currency_codes, $available_currencies);
    }
    // Stop rendering if there are no currencies available.
    if (empty($currency_codes)) {
      return $element;
    }

    $options = [];

    foreach ($currency_codes as $code) {
      $options[$code] = $currencies[$code]->getName();
    }

    $element['#tree'] = true;
    $element['#attributes']['class'][] = 'form-type-currency-field-currency';

    $last_visible_element = 'currency_code';
    $element['currency_code'] = [
      '#type' => 'select',
      '#title' => $element['#title'],
      '#title_display' => $element['#title_display'],
      '#default_value' => $default_value ? $default_value['currency_code'] : null,
      '#required' => $element['#required'],
      '#error_no_message' => true,
      '#options' => $options,
      '#field_suffix' => '',
    ];

    if (isset($element['#ajax'])) {
      $element['currency_code']['#ajax'] = $element['#ajax'];
    }

    // Add the help text if specified.
    if (!empty($element['#description'])) {
      $element[$last_visible_element]['#field_suffix'] .= '<div class="description">' . $element['#description'] . '</div>';
    }
    // Remove the keys that were transferred to child elements.
    unset($element['#ajax']);

    return $element;
  }

  /**
   * Validates the default value.
   *
   * @param mixed $default_value
   *   The default value.
   *
   * @return bool
   *   TRUE if the default value is valid, FALSE otherwise.
   */
  static function validateDefaultValue($default_value) {
    if (false === is_array($default_value)) {
      return false;
    }
    if (false === array_key_exists('currency_code', $default_value)) {
      return false;
    }
    return true;
  }

  /**
   * Moves inline errors from the "number" element to the main element.
   * This ensures that they are displayed in the right place
   * (below both number and currency_code, instead of between them).
   * Only performed when the inline_form_errors module is installed.
   *
   * @param array              $element
   *   The form element.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  static function moveInlineErrors(array $element, FormStateInterface $form_state) {
    $error = $form_state->getError($element['currency_code']);

    if (!empty($error) && !empty($element['#price_inline_errors'])) {
      $form_state->setError($element, $error);
    }
  }

}
